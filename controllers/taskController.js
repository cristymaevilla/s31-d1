

// all operations will be place here:

// importing model in controller using relative link; we don't need to include the .js extension:
const Task = require("../models/task");

// Controller function for getting all the tasks
module.exports.getAllTasks = ()=> {
		return Task.find({}).then(result => {
			return result;
		})
}

// CONTROLLER function for creating a task (createTask function):
module.exports.createTask = (requestBody)=> {
	// creates a task object based on the mongoose model "Task"
	let newTask = new Task({
		// sets the "name" property with the value recieved from the client(postman)
		name: requestBody.name
	})
	return newTask.save().then((task, error)=> {
		if (error){
			console.log(error);
			return false;
		}
		else{
			return task;
		}
	})
}

// Controller function for deleting a task
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			return removedTask;
		}
	})
}

// Controller function for updating a task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}

		result.name = newContent.name;

		return result.save().then((updatedTask, saveErr) => {
			if (saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updatedTask;
			}
		})
	})
}

// ACTIVITY=======================================================
// ROUTE for retrieving specific task: retrieveTask

module.exports.retrieveTask = (taskId) =>{
	return Task.findById(taskId).then((result,error)=>{
		if(error){
			console.log(error);
			return false;
		}
		else{ return result;}
	})
}
// ROUTE for changing status of task:
module.exports.changeStatus = (taskId, newStatus)=>{
	return Task.findById(taskId).then((result,error)=>
	{
		if(error){
			console.log(error);
			return false;
		}
		else{
			let newStatus = {status: "complete"}
		result.status = newStatus.status;
		 return result.save();

		}
	})
}

// let newStatus = "complete";
// 		 return result.status(newStatus);