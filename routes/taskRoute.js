// SEetup express dependecy
const express = require("express");
// create a router instance:
const router = express.Router();
// import the taskController.js
const taskController= require("../controllers/taskController");

// SETTING ROUTES-------------------------------------- 

// ROUTE to get all tasks:
// note: taskController is the filename located inside controllers folder
router.get("/", (req,res)=>{
		// note: "taskController" is the filename located inside controllers folder
	taskController.getAllTasks().then(resultFromController => res.send(
		resultFromController));
})

// Route to create a new task
router.post("/",(req, res)=> {
	taskController.createTask(req.body).then(resultFromController=> res.send(resultFromController));
})

// Route for deleting a task
// localhost:3001/tasks/09876b0987098y0
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route for updating a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


// ACTIVITY=========================================================================================
// ROUTE for retrieving specific task:
router.get("/:id", (req,res)=>{
	taskController.retrieveTask(req.params.id).then(resultFromController=> res.send(resultFromController));
})

// ROUTE for changing status of task:
router.put("/:id/complete", (req,res)=>{
	taskController.changeStatus(req.params.id).then(resultFromController => res.send(resultFromController));
})




module.exports = router;

