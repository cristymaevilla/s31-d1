
// Set up the dependencies:
const express = require("express");
const mongoose = require("mongoose");


// Import the task routes:
const taskRoute= require("./routes/taskRoute");

// server setup:
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// DATABASE CONNECTION:
// connecting to mongoDB Atlas
mongoose.connect("mongodb+srv://dbcristyvilla:D4pe7npdr9J6r7ct@wdc028-course-booking.ktimy.mongodb.net/b138_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}
);

// add the task route:
app.use("/tasks", taskRoute)


// server listening:
app.listen(port, ()=> console.log(`Now listening to port ${port}`));