
// Create the schema, model:
const mongoose = require("mongoose");


// SCHEMA:
const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type:String,
		default: "pending"
	}
});
// EXPORT the model:
module.exports = mongoose.model("Task", taskSchema);